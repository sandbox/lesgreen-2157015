<?php

/**
 * @file
 * Administrative page callbacks for the responsive menu modes module.
 */

/**
 * General configuration form for controlling the responsive menu behaviour.
 */
function responsive_menu_modes_admin_settings($form, &$form_state) {
  $path = drupal_get_path('module', 'responsive_menu_modes');
  //drupal_add_js($path . '/js/responsive_menu_modes_admin_settings.js', array('scope' => JS_DEFAULT));
  drupal_add_css($path . '/css/responsive_menu_modes_admin_settings.css');
  
  $responsive_menu_modes_path = responsive_menu_modes_get_path();

  $form['responsive_menu_modes_plugin_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Responsive Menu Plugin Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['responsive_menu_modes_plugin_settings']['responsive_menu_modes_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $responsive_menu_modes_path,
    '#description' => t('The location where gpResponsiveMenu plugin is installed. Relative paths are from the Drupal root directory.'),
    '#after_build' => array('_responsive_menu_modes_admin_settings_check_plugin_path'),
  );
  $form['responsive_menu_modes_plugin_settings']['responsive_menu_modes_compression_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose Responsive Menu compression level'),
    '#options' => array(
      'min' => t('Production (Minified)'),
      'none' => t('Development (Uncompressed Code)'),
    ),
    '#default_value' => variable_get('responsive_menu_modes_compression_type', 'min'),
  );
  $form['responsive_menu_modes_plugin_settings']['responsive_menu_modes_current_nav_container'] = array(
    '#type' => 'textfield',
    '#title' => t('Current navigation container'),
    '#default_value' => variable_get('responsive_menu_modes_current_nav_container', 'ul.menu'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('The container for the existing menu. This is the class or id of the ul element.'),
  );
  $form['responsive_menu_modes_plugin_settings']['responsive_menu_modes_current_nav_container_parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Current navigation parent container'),
    '#default_value' => variable_get('responsive_menu_modes_current_nav_container_parent', '#block-responsive-menu-modes-responsive-menu-modes-navigation'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('The parent container for the existing menu.'),
  );
  
  $form['responsive_menu_modes_block_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Responsive Menu Block Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['responsive_menu_modes_block_settings']['responsive_menu_modes_block_menu'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Menu'),
    '#default_value' => variable_get('responsive_menu_modes_block_menu', 'main-menu'),
    '#description' => t('Using the Responsive Menu block is optional. If used, please enter the menu that will be displayed in the block.'),
  );
  
  $form['responsive_menu_modes_navigation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['responsive_menu_modes_navigation_settings']['responsive_menu_modes_nav_parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Parent'),
    '#default_value' => variable_get('responsive_menu_modes_nav_parent', '#menu-bar'),
    '#size' => 50,
    '#required' => TRUE,
    '#description' => t('The container where the menu will be placed.'),
  );
  $form['responsive_menu_modes_navigation_settings']['responsive_menu_modes_nav_cntnr_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Container Class'),
    '#default_value' => variable_get('responsive_menu_modes_nav_cntnr_class', 'gp-responsive-nav-cntnr'),
    '#size' => 50,
    '#description' => t('The class of the menu container.'),
  );
  $form['responsive_menu_modes_navigation_settings']['responsive_menu_modes_nav_position'] = array(
    '#type' => 'radios',
    '#title' => t('Menu Position'),
    '#options' => array(0 => t('First'), 1 => t('Last')),
    '#default_value' => variable_get('responsive_menu_modes_nav_position', 1),
    '#description' => t('Whether the navigation menu will be the first or last element in the container.'),
  );
  $form['responsive_menu_modes_navigation_settings']['responsive_menu_modes_show_nav_caret'] = array(
    '#type' => 'radios',
    '#title' => t('Show Navigation Caret'),
    '#options' => array(0 => t('False'), 1 => t('True')),
    '#default_value' => variable_get('responsive_menu_modes_show_nav_caret', 1),
    '#description' => t('Whether to display a caret to the right of the menu items that have sub-menus. '),
  );
  
  $form['responsive_menu_modes_toggle_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Toggle Button Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );  
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_toggle_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Navigation Toggle Button Type'),
    '#options' => array(0 => t('Icon'), 1 => t('Text')),
    '#default_value' => variable_get('responsive_menu_modes_toggle_button_type', 0),
    '#description' => t('Select whether to display an icon or text.'),
    '#prefix' => '<div class="responsive-menu-toggle-button-type">',
    '#suffix' => '</div>',
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Toggle Button Text'),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_text', 'MENU'),
    '#size' => 50,
    '#description' => t('The menu toggle button text.'),
    '#states' => array(
      'visible' => array(
        ':input[name="responsive_menu_modes_toggle_button_type"]' => array('value' => '1'),
      ),
    ),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_show_nav_toggle_caret'] = array(
    '#type' => 'radios',
    '#title' => t('Toggle Button Caret'),
    '#options' => array(0 => t('False'), 1 => t('True')),
    '#default_value' => variable_get('responsive_menu_modes_show_nav_toggle_caret', 1),
    '#description' => t('Whether to display a caret to the right of the toggle button menu text. '),
    '#states' => array(
      'visible' => array(
        ':input[name="responsive_menu_modes_toggle_button_type"]' => array('value' => '1'),
      ),
    ),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_icon_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Toggle Button Icon Class'),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_icon_class', 'gp-responsive-menu-icon gp-responsive-icon-y'),
    '#size' => 50,
    '#description' => t('The menu toggle button icon class. This value should be blank if you want to use the Toggle Button Text'),
    '#states' => array(
      'visible' => array(
        ':input[name="responsive_menu_modes_toggle_button_type"]' => array('value' => '0'),
      ),
    ),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Toggle Button Class'),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_class', 'gp-responsive-nav-toggle'),
    '#size' => 50,
    '#description' => t('An addtional class setting for the menu toggle button.'),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Toggle Button Parent'),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_parent', ''),
    '#size' => 50,
    '#description' => t('By default, the toggle button will be placed in the same container as the navigation menu. Set this value if you want the button to be placed in a different location.'),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_wrapper_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Toggle Button Wrapper Class'),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_wrapper_class', 'gp-responsive-toggle-wrapper'),
    '#size' => 50,
    '#description' => t('Set this value if you want the toggle button to be wrapped in an additional container.'),
  );
  $form['responsive_menu_modes_toggle_settings']['responsive_menu_modes_nav_toggle_position'] = array(
    '#type' => 'radios',
    '#title' => t('Toggle Button Position'),
    '#options' => array(0 => t('First'), 1 => t('Last')),
    '#default_value' => variable_get('responsive_menu_modes_nav_toggle_position', 0),
    '#description' => t('Whether the toggle button will be the first or last element in the container.'),
  );
  
  $form['responsive_menu_modes_media_query_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Media Query Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_media_query_wrapper'] = array(
  	'#tree' => TRUE,
    '#prefix' => '<div id="responsive-menu-media-query-wrapper">',
    '#suffix' => '</div>',
    '#theme' => 'responsive_menu_modes_media_query_modes',
  );

  $delta = 0;
  $modes = variable_get('responsive_menu_modes_media_query_modes', array());
  if (isset($form_state['media_query_count'])) {
    $media_query_count = $form_state['media_query_count'];
	if (isset($form_state['responsive_menu_modes_media_query']->queryModes)) {
	  $modes = $form_state['responsive_menu_modes_media_query']->queryModes;	
	}
  }
  else {
    $media_query_count = max(1, empty($modes) ? 1 : count($modes)); 
  }

  if ($media_query_count > 0) {
    foreach ($modes as $key => $mode) {
      $delta = $key;
  	  $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_media_query_wrapper'][$key] = _responsive_menu_modes_media_query_modes_form($key, $mode['responsive_menu_modes_media_query_min'], $mode['responsive_menu_modes_media_query_max'], $mode['responsive_menu_modes_media_query_mode']);
	  $delta++;
    }
	
  }
  if (($delta == 0) || (isset($form_state['media_query_count']))) {
    $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_media_query_wrapper'][$delta] = _responsive_menu_modes_media_query_modes_form($delta, '', '', '');
  }
  
  $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_media_query_modes_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add More Modes'),
    '#submit' => array('responsive_menu_modes_media_query_add_mode'),
    '#ajax' => array(
      'callback' => 'responsive_menu_modes_media_query_modes_js',
      'wrapper' => 'responsive-menu-media-query-wrapper',
    ),
  );
  
  $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_query_mode_order_help'] = array(
    '#markup' => '<p><strong>Note:</strong> The media query sizes should be ordered from largest to smallest. The minimum value is not required for the last item.<br /> All deletes are final.</p>',
  );
    
  $form['responsive_menu_modes_extra_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['responsive_menu_modes_extra_settings']['responsive_menu_modes_menu_select_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Select Default Text'),
    '#default_value' => variable_get('responsive_menu_modes_menu_select_text', 'MENU'),
    '#size' => 50,
    '#description' => t('Used with Select mode. The default text.'),
  );
  $form['responsive_menu_modes_extra_settings']['responsive_menu_modes_page_wrapper'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Wrapper'),
    '#default_value' => variable_get('responsive_menu_modes_page_wrapper', '.wrapper'),
    '#size' => 50,
    '#description' => t('Used with SplitView mode. The container for all content on the page.'),
  );
  $form['responsive_menu_modes_extra_settings']['responsive_menu_modes_parent_menu_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent Menu Prefix'),
    '#default_value' => variable_get('responsive_menu_modes_parent_menu_prefix', 'All '),
    '#size' => 50,
    '#description' => t('When a menu item has children, but also has a url, the menu item will be added a child with this prefix.'),
  );
  $form['responsive_menu_modes_extra_settings']['responsive_menu_modes_active_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Active Class'),
    '#default_value' => variable_get('responsive_menu_modes_active_class', 'active'),
    '#size' => 50,
    '#description' => t('Used with SplitView and parent menu items.'),
  );
  $form['responsive_menu_modes_extra_settings']['responsive_menu_modes_show_close_btn'] = array(
    '#type' => 'radios',
    '#title' => t('Show Close Button'),
    '#options' => array(0 => t('False'), 1 => t('True')),
    '#default_value' => variable_get('responsive_menu_modes_show_close_btn', 0),
    '#description' => t('Used with SplitView mode. Whether to display a close button above the SplitView menu panel. '),
  );
  $form['#submit'][] = 'responsive_menu_modes_settings_submit';
  return system_settings_form($form);
}

function _responsive_menu_modes_media_query_modes_form($key, $min, $max, $mode) {
  $responsive_modes = array(
    'InlineOverlay' => t('InlineOverlay'),
    'PageOverlay' => t('PageOverlay'),
    'Select' => t('Select'),
    'SplitView' => t('SplitView'),
    'Vertical' => t('Vertical'),
    'VerticalOverlay' => t('VerticalOverlay'),
  );
  $form = array(
    '#tree' => TRUE,
  );
  $form['responsive_menu_modes_media_query_min'] = array(
    '#type' => 'textfield',
	'#title' => t('Min'),
	'#title_display' => 'invisible',
	'#size' => 20,
	'#default_value' => $min,
  );
  $form['responsive_menu_modes_media_query_max'] = array(
    '#type' => 'textfield',
	'#title' => t('Max'),
    '#title_display' => 'invisible',
	'#size' => 20,
	'#required' => TRUE,
	'#default_value' => $max,
   );
   $form['responsive_menu_modes_media_query_mode'] = array(
     '#type' => 'select',
     '#title' => t('Mode'),
     '#title_display' => 'invisible',
     '#options' => $responsive_modes,
     '#default_value' => $mode, 
   );
   
   $form['responsive_menu_modes_media_query_delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#id' => 'responsive-menu-delete-mode-'.$key,
    '#submit' => array('responsive_menu_modes_media_query_delete_mode'),
    '#ajax' => array(
      'callback' => 'responsive_menu_modes_media_query_delete_mode_callback',
    ),
  );  
  
  return $form;
}

/**
 * Ajax callback in response to new tab being added to the form.
 *
 * This returns the new page content to replace the page content made obsolete
 * by the form submission.
 *
 * @see responsive_menu_modes_media_query_more_modes_submit()
 */
function responsive_menu_modes_media_query_modes_js($form, $form_state) {
  return $form['responsive_menu_modes_media_query_settings']['responsive_menu_modes_media_query_wrapper'];
}

function responsive_menu_modes_media_query_add_mode($form, &$form_state) {
  if ($form_state['values']['responsive_menu_modes_media_query_modes_more']) {
    $form_state['media_query_count'] = count($form_state['values']['responsive_menu_modes_media_query_wrapper']) +1;
  }
  $form_state['responsive_menu_modes_media_query']->queryModes = array_values($form_state['values']['responsive_menu_modes_media_query_wrapper']);
  unset($form_state['input']['responsive_menu_modes_media_query_wrapper']);
  $form_state['rebuild'] = TRUE;
}

function responsive_menu_modes_media_query_delete_mode_callback($form, $form_state) {
  $ln = count(array_values($form_state['values']['responsive_menu_modes_media_query_wrapper']));
  if ($ln > 1) {
  	
	
    $index = _responsive_menu_modes_get_delete_button_index($form_state);
    $commands = array();
    $commands[] = ajax_command_remove('#responsive-menu-media-query-modes-table tr.responsive-menu-row-' . $index);
    $commands[] = ajax_command_restripe('#responsive-menu-media-query-modes-table');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  
}
function responsive_menu_modes_media_query_delete_mode($form, &$form_state) {
  $modes = $form_state['values']['responsive_menu_modes_media_query_wrapper'];
  $ln = count(array_values($modes));
  if ($ln > 1) {
  	$index = _responsive_menu_modes_get_delete_button_index($form_state);
	unset($modes[$index]);
	$form_state['responsive_menu_modes_media_query']->queryModes = $modes;
	//$form_state['media_query_count'] = count($modes);
	variable_set('responsive_menu_modes_media_query_modes', $modes);
	$form_state['rebuild'] = TRUE;
  }
}

function _responsive_menu_modes_get_delete_button_index($form_state) {
  $btn = $form_state['triggering_element']['#id'];
  $id = explode('-', $btn);
  $index = $id[4];
  return $index;
}

function theme_responsive_menu_modes_media_query_modes($vars) {
  $element = $vars['element'];
  drupal_add_tabledrag('responsive-menu-media-query-modes-table', 'order', 'sibling', 'mode-weight');
  $delta = 0;
  $rows = array();
  $header = array(
    'responsive_menu_modes_media_query_min' => t('Min'), 
    'responsive_menu_modes_media_query_max' => t('Max'),
    'responsive_menu_modes_media_query_mode' => t('Mode'),
    'responsive_menu_modes_media_query_delete' => t('Delete'),
    //'weight' => t('Weight'),
  );
  foreach (element_children($element) as $key) {
    $row = array();
    $row['data'] = array();
    foreach ($header as $fieldname => $title) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
	  $row['class'] = array('responsive-menu-row-'.$key);
     // $row['class'] = array('draggable'); // needed for table dragging
    }
    $rows[] = $row;
	$delta++;
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'responsive-menu-media-query-modes-table')));
}

/**
 * Checks if the directory in $form_element exists and contains a file named
 * 'jquery.gpresponsivemenu.js'. If validation fails, the form element is flagged
 * with an error from within the file_check_directory function.
 *
 * @param $form_element
 *   The form element containing the name of the directory to check.
 */
function _responsive_menu_modes_admin_settings_check_plugin_path($form_element) {
  $library_path = $form_element['#value'];
  if (!is_dir($library_path) || !(file_exists($library_path . '/js/jquery.gpresponsivemenu.js') && file_exists($library_path . '/js/jquery.gpresponsivemenu.min.js'))) {
  	if ($library_path == '') {
		$library_path = 'libraries/gpresponsivemenu/';
	}
    form_set_error($form_element['#parents'][0], t('You need to download the !responsive_menu_modes and extract the entire contents of the archive into the %path folder of your server. Please make sure that the folder and file names are all lowercase.', array('!responsive_menu_modes' => l(t('gpResponsiveMenu plugin'), 'https://github.com/lesgreen/gpResponsiveMenu'), '%path' => $library_path)));
  }

  return $form_element;
}

function responsive_menu_modes_settings_submit($form, &$form_state) {
	variable_set('responsive_menu_modes_media_query_modes', array_values($form_state['values']['responsive_menu_modes_media_query_wrapper']));
}
