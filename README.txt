INTRODUCTION
------------
gpResponsiveMenu is a customizable responsive menu plugin for jQuery. The Responsive Menu Modes module allows for integration of gpResponsiveMenu into Drupal.

OVERVIEW
--------
1.  Can be used as a stand-alone menuing system
2.  Can be used with other menu modules such as Superfish
3.  Has 6 navigation options (modes) that displayed using media queries
        Select (combo box)
        Vertical
        VerticalOverlay
        InlineOverlay
        SplitView
        PageOverlay
4.  Multiple modes can be used on a page
5.  Plugin comes with 20 menu icons (or create your own)


REQUIREMENTS
------------

gpResponsiveMenu Plugin (https://github.com/lesgreen/gpResponsiveMenu)

Libraies Module
