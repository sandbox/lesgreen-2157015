(function ($) {
	Drupal.behaviors.responsiveMenuDefaultNav = {
  		attach: function (context, settings) {
  			var a;
			var li = $('#block-responsive-menu-responsive-menu-navigation ul.menu li:has(ul)');
			$.each($(li), function(i, itm) {
				$(this).addClass('responsive-default-menu-has-sub');
				a = $(this).children('a')[0];
		        $(a).append('<b class="responsive-menu-nav-caret" />');
			});
			$('li.responsive-default-menu-has-sub a').click(function(e) {
				$(this).toggleClass('active-ul');
				$(this).parent().children('ul').toggle();
				e.preventDefault();
			});
  		}
	};	
})(jQuery);